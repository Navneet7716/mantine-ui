import { Container } from "@mantine/core";
import Form from "./Form";

import "./Home.css";
import NavBar from "./NavBar";

function Home() {
  return (
    <div>
      <NavBar />
      <Container size="xl" padding="sm" className="Main-Container">
        <Form />
      </Container>
    </div>
  );
}

export default Home;
