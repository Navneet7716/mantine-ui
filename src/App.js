import {
  ColorSchemeProvider,
  MantineProvider,
} from "@mantine/core";
import { useState } from "react";
import Helmet from "react-helmet";
import Home from "./Home";

function App() {
  const [colorScheme, setColorScheme] = useState("light");
  const toggleColorScheme = (value) => {
    setColorScheme(value || (colorScheme === "dark" ? "light" : "dark"));
  };
  const dark = colorScheme === "dark";

  console.log(dark);

  return (
    <ColorSchemeProvider
      colorScheme={colorScheme}
      toggleColorScheme={toggleColorScheme}
    >
      <MantineProvider theme={{ colorScheme }}>
        <Helmet>
          <style>{`body { background-color: ${
            dark ? "rgb(26, 27, 30)" : "white"
          }; }`}</style>
        </Helmet>
        <Home />
      </MantineProvider>
    </ColorSchemeProvider>
  );
}

export default App;
